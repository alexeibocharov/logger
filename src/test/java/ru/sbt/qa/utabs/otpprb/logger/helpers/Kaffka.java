package ru.sbt.qa.utabs.otpprb.logger.helpers;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import ru.sbtqa.tag.qautils.errors.AutotestError;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.UUID;


public class Kaffka extends Thread {

    public static String TEMPLATE_DIR = System.getProperty("user.dir") + "\\src\\test\\resources\\templates\\";
//    public static String KAFKA_SERVER_URL = "localhost";
//    public static int KAFKA_SERVER_PORT = 9092;
    public static String KAFKA_SERVER_URL = "10.116.178.205";
    public static int KAFKA_SERVER_PORT = 9093;

    public void SendToKaffka(String topic, String templateName) {
        // берем файлик и парсим его в стрингу
        String fileName = TEMPLATE_DIR + templateName;
        try {
            String message = new String(Files.readAllBytes(Paths.get(fileName)));
            // обьявляем продюсер и шлём полученную строчку
            Properties properties = new Properties();
            properties.put("bootstrap.servers", KAFKA_SERVER_URL + ":" + KAFKA_SERVER_PORT);
            properties.put("client.id", "test-producer-" + UUID.randomUUID());
            properties.put("group.id", UUID.randomUUID().toString());
            properties.put("key.serializer", "org.apache.kafka.common.serialization.IntegerSerializer");
            properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
            properties.put("key.deserializer", StringDeserializer.class.getName());
            properties.put("value.deserializer", StringDeserializer.class.getName());
            properties.put("ask", "all");
            properties.put("security.protocol", "SSL");
            properties.put("ssl.truststore.location", "C:\\Users\\bocharov-ao\\keystores\\server.truststore.jks");
            properties.put("ssl.truststore.password", "qwe123");
            properties.put("ssl.truststore.type", "JKS");
            KafkaProducer<String, String> producer = new KafkaProducer<>(properties);
            for (int i = 0 ;i <100; i++) {
                producer.send(new ProducerRecord<>(topic, message));
            };
//            producer.send(new ProducerRecord<>(topic,2,null,message));
            // вычитаем топик для проверки
//            final Consumer<String,String> consumer = new KafkaConsumer<>(properties);
//            List<String> topics = new LinkedList<>();
//            topics.add(topic);
//            System.out.println(topics);
//            consumer.subscribe(topics);
//            final int giveUp = 2 ; int noRecordsCount= 0 ;
//                while (true){
//                final ConsumerRecords<String,String> consumerRecord = consumer.poll(10);
//                if (consumerRecord.count() == 0){
//                    noRecordsCount++;
//                    if (noRecordsCount > giveUp) break;
//                    else continue;
//                }
//            consumerRecord.forEach(record -> {record.value(); /*.contains("cleaner")*/ {
//                System.out.printf("Consumer Record:(%d,%s,%d,%d)\n," +
//                                record.key(), record.value(),
//                        record.partition(), record.offset());
//            }});
//            }
        } catch (IOException ioe) {
            throw new AutotestError("Не нашли файлик", ioe);
        }
    }
}

