package ru.sbt.qa.utabs.otpprb.logger.helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.extensions.DriverExtension;
import ru.sbtqa.tag.qautils.errors.AutotestError;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Lists {

    private static final String optionsXPath = "//div[contains(@class,'selectbox__item') or contains(@class,'dropdown__list-item')]";

    public static String getLabel(WebElement element) {
        return element.findElement(By.xpath(".//div[contains(@class, 'field__label')]")).getText();
    }

    public static List<String> getListValues(WebElement element) {
        element.click();
        List<String> listValues = getOptions().stream().filter((w) -> {
            return !w.getAttribute("class").contains("first");
        }).map(WebElement::getText).collect(Collectors.toList());
        element.findElement(By.xpath(".//div[contains(@class, 'field__label')]")).click();
        return listValues;
    }

    public static String getTextFocused(WebElement element) {
        return element.findElement(By.xpath(".//div[contains(@class,'selectbox__value')]")).getText();
    }

    public static void selectByValue(WebElement element, String value) {
        element.click();
        Map<String, WebElement> options = getOptionsMappedToStrings();
        if (options.isEmpty()) {
            throw new AutotestError("SelectBox is empty");
        } else if (!options.containsKey(value)) {
            throw new AutotestError("SelectBox has no option: " + value);
        } else {
            options.get(value).click();
        }
    }

    private static Map<String, WebElement> getOptionsMappedToStrings() {
        Map<String, WebElement> result = new HashMap();
        Iterator var2 = getOptions().iterator();

        while(var2.hasNext()) {
            WebElement element = (WebElement)var2.next();
            result.put(element.getText(), element);
        }

        return result;
    }

    private static List<WebElement> getOptions() {
        DriverExtension.waitUntilElementAppearsInDom(By.xpath(optionsXPath));
        return PageFactory.getWebDriver().findElements(By.xpath(optionsXPath));
    }
}