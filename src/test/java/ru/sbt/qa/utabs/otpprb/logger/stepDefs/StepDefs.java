package ru.sbt.qa.utabs.otpprb.logger.stepDefs;

import cucumber.api.DataTable;
import cucumber.api.java.ru.Когда;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import ru.sbt.qa.utabs.otpprb.logger.helpers.Tools;
import ru.sbtqa.tag.allurehelper.ParamsHelper;
import ru.sbtqa.tag.datajack.Stash;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.exceptions.PageException;
import ru.sbtqa.tag.qautils.errors.AutotestError;

import java.util.Map;

public class StepDefs {

    @Когда("^пользователь добавляет в MultiCombobox значения$")
    public void addMultiComboboxValues(DataTable dataTable) {
        dataTable.getPickleRows().forEach(r -> {
            try {
                String elementTitle = GenerateRandTestData.checkTestData(r.getCells().get(0).getValue(), r.getCells().get(0).getValue());
                switchTab(elementTitle);
                String value = GenerateRandTestData.checkTestData(r.getCells().get(0).getValue(), r.getCells().get(1).getValue());
                WebElement element = PageFactory.getInstance().getCurrentPage().getElementByTitle(Tools.mapToStashedValueIfPossible(elementTitle));
                WebElement inputElement = Tools.getInput(element);
                Tools.clearElement(inputElement);
                inputElement.sendKeys(Tools.mapToStashedValueIfPossible(value));
                inputElement.sendKeys(Keys.ENTER);
            } catch (Exception ex) {
                throw new AutotestError(String.format("Не удалось найти поле '%s', либо поле имеет некорректный тип", r.toString()), ex);
            }
        });
    }
/*
    @Когда("^пользователь добавляет в MultiCombobox значения из файла$")
    public void addMultiComboboxValuesFromFile(DataTable dataTable) {
        dataTable.getPickleRows().forEach(r -> {
            try {
                String elementTitle = GenerateRandTestData.checkTestData(r.getCells().get(0).getValue(), r.getCells().get(0).getValue());
                switchTab(elementTitle);
                String value = GenerateRandTestData.checkTestData(r.getCells().get(0).getValue(), r.getCells().get(1).getValue());
                WebElement element = PageFactory.getInstance().getCurrentPage().getElementByTitle(Tools.mapToStashedValueIfPossible(elementTitle));
                WebElement inputElement = Tools.getInput(element);
                Tools.clearElement(inputElement);
                inputElement.sendKeys(Tools.mapToStashedValueIfPossible(value));
                inputElement.sendKeys(Keys.ENTER);
            } catch (Exception ex) {
                throw new AutotestError(String.format("Не удалось найти поле '%s', либо поле имеет некорректный тип", r.toString()), ex);
            }
        });
    }
*/
    @Когда("^(?:пользователь |он |)заполняет (?:поля данными|поля|поле данными|поля на странице)$")
    public void fillMultipleFields(DataTable dataTable) {
        Map<String, String> dataMap = dataTable.asMap(String.class, String.class);
        dataMap.forEach((k, v) -> {
            try {
                switchTab(k);
                v = GenerateRandTestData.checkTestData(k, v);
                WebElement element = PageFactory.getInstance().getCurrentPage().getElementByTitle(k);
                WebElement inputElement = Tools.getInput(element);
                Tools.clearElement(inputElement);
                inputElement.sendKeys(Tools.mapToStashedValueIfPossible(v));
                inputElement.sendKeys(Keys.TAB);
            } catch (Exception ex) {
                throw new AutotestError(String.format("Не удалось найти поле '%s', либо поле имеет некорректный тип", k), ex);
            }
        });
    }

    @Когда("^(?:пользователь |он |)нажимает на (?:ссылку |чекбокс |кнопку |элемент )\"(.*?)\"$")
    public static void clickOnElement(String elementName) {
        ParamsHelper.addParam("нажимает на элемент", elementName);
        try {
            switchTab(elementName);
            WebElement element = PageFactory.getInstance().getCurrentPage().getElementByTitle(elementName);
            element.click();
        } catch (PageException | NoSuchElementException ex) {
            throw new AutotestError(ex);
        }
    }

    public static void switchTab(String elementName) {
        if (elementName.startsWith("Параметры отображения ")) {
            clickOnElement("Таб Параметры отображения");
        }
        if (elementName.startsWith("Фильтры ")) {
            clickOnElement("Таб Фильтры");
        }
        try {
            Thread.sleep(500);
        } catch (Exception ex) {
            //TODO: remove thread sleep!
        }
    }

}
