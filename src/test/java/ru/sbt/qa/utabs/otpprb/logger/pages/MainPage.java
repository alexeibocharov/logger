package ru.sbt.qa.utabs.otpprb.logger.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import ru.sbtqa.tag.pagefactory.annotations.ActionTitle;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.sbtqa.tag.pagefactory.extensions.DriverExtension;
import ru.yandex.qatools.htmlelements.element.Image;

/**
 * Created by sbt-kiyashenko-io on 18.08.2017.
 */
@PageEntry(title = "Главная страница")
public class MainPage extends CommonPage {
    @ElementTitle("На главную")
    @FindBy(xpath = "//*[contains(@class,'icon_img-sberbank-logo')]")
    public Image iconSberbank;

    public MainPage() {
        super();
        waitForEventCaptureOverlayDisappear();
    }

    @ActionTitle("выбирает блок")
    public void clickByBlock(String name) {
        String blockXpath = "//*[contains(text(),'" + name + "')]";
        DriverExtension.waitUntilElementAppearsInDom(By.xpath(blockXpath)).click();
    }

    public void blockExists(String name) {
        String blockXpath = "//*[contains(text(),'" + name + "')]";
        DriverExtension.waitUntilElementAppearsInDom(By.xpath(blockXpath)).findElement(By.xpath(blockXpath)).isEnabled();
    }
}
