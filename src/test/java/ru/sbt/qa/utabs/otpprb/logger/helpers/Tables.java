package ru.sbt.qa.utabs.otpprb.logger.helpers;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ru.sbtqa.tag.qautils.errors.AutotestError;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class Tables {

    public static List<WebElement> getHeadings(WebElement element) {
        return element.findElements(By.xpath(".//thead[contains(@class,'table__thead')]//td"));
    }

    public static List<WebElement> getColumnByHeading(WebElement element, String heading) {
        List<WebElement> headings = getHeadings(element);

        for (int i = 0; i < headings.size(); ++i) {
            if (heading.equals((headings.get(i)).getText())) {
                WebElement var10000 = element;
                StringBuilder var10001 = (new StringBuilder()).append(".//td[contains(@class, 'table__cell') and not(contains(@class, 'table__cell_header'))][");
                ++i;
                return var10000.findElements(By.xpath(var10001.append(i).append("]//*[text()]").toString()));
            }
        }
        throw new AutotestError("Heading \"" + heading + "\" is not found");
    }

    public static List<List<WebElement>> getRows(WebElement element) {
        List<List<WebElement>> rowList = new ArrayList();
        Iterator var3 = element.findElements(By.xpath(".//tbody//tr[contains(@class,'table__row')]")).iterator();

        while (var3.hasNext()) {
            WebElement column = (WebElement) var3.next();
            List<WebElement> row = column.findElements(By.xpath(".//td"));
            if (!row.isEmpty()) {
                rowList.add(row);
            }
        }
        return rowList;
    }

    public static List<List<String>> getRowsAsTextButEmptyAndDate(WebElement table) {
        List<List<String>> rowsAsText = new ArrayList<>();
        List<List<WebElement>> rows = getRows(table);
        if (!rows.isEmpty()) {
            return rows.stream().map(e -> e.stream().map(WebElement::getText).filter(t -> !t.trim().isEmpty() && !Tools.isDate(t.trim())).collect(Collectors.toList())).collect(Collectors.toList());
        }
        return rowsAsText;
    }

    public static List<List<String>> getRowsAsTextButEmpty(WebElement table) {
        List<List<String>> rowsAsText = new ArrayList<>();
        List<List<WebElement>> rows = getRows(table);
        if (!rows.isEmpty()) {
            return rows.stream().map(e -> e.stream().map(c -> Tools.formatCellValue(c.getText())).filter(c -> !c.isEmpty()).collect(Collectors.toList())).collect(Collectors.toList());
        }
        return rowsAsText;
    }

    public static void checkLables(WebElement table, List<String> labelsOfDisplaySettings) {
        Assert.assertTrue("Размеры таблицы и данных не совпадают", labelsOfDisplaySettings.size() == getHeadings(table).size());

        for (int i = 0; i < labelsOfDisplaySettings.size(); ++i) {
            Assert.assertTrue("Не совпал: " + (String) labelsOfDisplaySettings.get(i), ((String) labelsOfDisplaySettings.get(i)).equals(((WebElement) getHeadings(table).get(i)).getText().trim()));
        }
    }

    public static List<WebElement> getColumnByIndex(WebElement table, int index) {
        return table.findElements(By.xpath("//td[contains(@class, 'table__cell')][" + index + "]"));
    }
}
