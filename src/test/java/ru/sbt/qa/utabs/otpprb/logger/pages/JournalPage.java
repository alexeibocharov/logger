package ru.sbt.qa.utabs.otpprb.logger.pages;

import cucumber.api.DataTable;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import ru.sbt.qa.utabs.otpprb.logger.helpers.Constants;
import ru.sbt.qa.utabs.otpprb.logger.helpers.MultiComboBoxes;
import ru.sbt.qa.utabs.otpprb.logger.helpers.Tables;
import ru.sbt.qa.utabs.otpprb.logger.helpers.Tools;
import ru.sbt.qa.utabs.otpprb.logger.utils.ExcelUtil;
import ru.sbtqa.tag.datajack.Stash;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.annotations.ActionTitle;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.exceptions.PageException;
import ru.sbtqa.tag.pagefactory.extensions.DriverExtension;
import ru.sbtqa.tag.qautils.errors.AutotestError;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static ru.sbt.qa.utabs.otpprb.logger.utils.AllureHelper.toAllure;


/**
 * Created by sbt-kiyashenko-io on 24.10.2017.
 */
public class JournalPage extends CommonPage {

    Logger logger = Logger.getLogger(JournalPage.class.getName());

    @ElementTitle("Таб Фильтры")
    @FindBy(xpath = "//div[contains(@class, 'tabs__item') and contains(text(), 'Фильтры')]")
    public WebElement tabFilters;

    @ElementTitle("Таб Параметры отображения")
    @FindBy(xpath = "//div[contains(@class, 'tabs__item') and contains(text(), 'Параметры отображения')]")
    public WebElement tabViewParams;

    @ElementTitle("Выйти из системы")
    @FindBy(xpath = "//span[contains(@class, 'toolbar__decorator') and text()='Выйти']/..")
    public WebElement btnExitOnSystem;

    @ElementTitle("Таблица")
    @FindBy(xpath = "//*[contains(@class, 'table_has-pagination')]")
    public WebElement table;

    @ElementTitle("Фильтры Начальная дата")
    @FindBy(xpath = "//div[text()='Начальная дата']/..")
    public WebElement startDate;

    @ElementTitle("Фильтры Начальное время")
    @FindBy(xpath = "//div[text()='Начальное время']/..")
    public WebElement startTime;

    @ElementTitle("Фильтры Конечная дата")
    @FindBy(xpath = "//div[text()='Конечная дата']/..")
    public WebElement endDate;

    @ElementTitle("Фильтры Конечное время")
    @FindBy(xpath = "//div[text()='Конечное время']/..")
    public WebElement endTime;

    @ElementTitle("Фильтры поиск")
    @FindBy(xpath = "//*[text()='Фильтры']/../..//*[text()='Поиск']/ancestor::button")
    public WebElement btnSearch;

    @ElementTitle("Фильтры Очистить")
    @FindBy(xpath = "//*[text()='Фильтры']/../..//*[text()='Очистить']/ancestor::button")
    public WebElement btnClear;

    @ElementTitle("Параметры отображения строк на страницу")
    @FindBy(xpath = "//*[text()='Параметры отображения']/../..//*[text()='Строк на страницу']/..")
    public WebElement stringsOnPageInput;

    @ElementTitle("Параметры отображения часовой пояс")
    @FindBy(xpath = "//*[text()='Параметры отображения']/../..//*[text()='Часовой пояс']/..")
    public WebElement timeZoneInput;

    @ElementTitle("Параметры отображения применить")
    @FindBy(xpath = "//*[text()='Параметры отображения']/../..//*[text()='Применить']/ancestor::button")
    public WebElement btnApply;

    @ElementTitle("Фильтры Стенд (ID)")
    @FindBy(xpath = "//div[text()='Стенд (ID)']" + Constants.XPATH_FILTER_FIELD)
    public WebElement filterStandID;

    @ElementTitle("Фильтры Модуль")
    @FindBy(xpath = "//div[text()='Модуль']" + Constants.XPATH_FILTER_FIELD)
    public WebElement filterModule;

    @ElementTitle("Фильтры Узел (ID)")
    @FindBy(xpath = "//div[text()='Узел (ID)']" + Constants.XPATH_FILTER_FIELD)
    public WebElement filterNodeID;

    @ElementTitle("Фильтры Узел (IP-адрес)")
    @FindBy(xpath = "//div[text()='Узел (IP-адрес)']" + Constants.XPATH_FILTER_FIELD)
    public WebElement filterNodeIP;

    @ElementTitle("Фильтры Узел (host name)")
    @FindBy(xpath = "//div[text()='Узел (host name)']" + Constants.XPATH_FILTER_FIELD)
    public WebElement filterNodeHostName;

    @ElementTitle("Фильтры Сессия")
    @FindBy(xpath = "//div[text()='Сессия']" + Constants.XPATH_FILTER_FIELD)
    public WebElement filterSession;

    @ElementTitle("Фильтры Пользователь")
    @FindBy(xpath = "//div[text()='Пользователь']" + Constants.XPATH_FILTER_FIELD)
    public WebElement filterUser;

    @ElementTitle("Фильтры Запрос")
    @FindBy(xpath = "//div[text()='Запрос']" + Constants.XPATH_FILTER_FIELD)
    public WebElement filterQuery;

    @ElementTitle("Фильтры Глубина")
    @FindBy(xpath = "//div[text()='Глубина']" + Constants.XPATH_FILTER_FIELD)
    public WebElement filterDeep;

    @ElementTitle("Фильтры Контейнер")
    @FindBy(xpath = "//div[text()='Контейнер']" + Constants.XPATH_FILTER_FIELD)
    public WebElement filterContainer;

    @ElementTitle("Фильтры Категория")
    @FindBy(xpath = "//div[text()='Категория']" + Constants.XPATH_FILTER_FIELD)
    public WebElement filterCategory;

    @ElementTitle("Фильтры Java Thread")
    @FindBy(xpath = "//div[text()='Java Thread']" + Constants.XPATH_FILTER_FIELD)
    public WebElement filterJavaThread;

    @ElementTitle("Фильтры Java Class")
    @FindBy(xpath = "//div[text()='Java Class']" + Constants.XPATH_FILTER_FIELD)
    public WebElement filterJavaClass;

    @ElementTitle("Фильтры Java Method")
    @FindBy(xpath = "//div[text()='Java Method']" + Constants.XPATH_FILTER_FIELD)
    public WebElement filterJavaMethod;

    @ElementTitle("Фильтры Java Line")
    @FindBy(xpath = "//div[text()='Java Line']" + Constants.XPATH_FILTER_FIELD)
    public WebElement filterJavaLine;

    @ElementTitle("Фильтры Мин. уровень")
    @FindBy(xpath = "//div[text()='Мин. уровень']" + Constants.XPATH_FILTER_FIELD)
    public WebElement filterMinLevel;

    @ElementTitle("Фильтры Текст")
    @FindBy(xpath = "//div[contains(text(),'Текст')]" + Constants.XPATH_FILTER_FIELD)
    public WebElement filterText;

    @ElementTitle("Настройки таблицы")
    @FindBy(xpath = "//span[text()='Настройки таблицы']")
    public WebElement btnSettingsTable;

    @ElementTitle("Найдено событий в таблице")
    @FindBy(xpath = "//div[contains(@class,'boardbar__')]//span[contains(text(),'найдено событий:')]")
    public WebElement eventsFoundInTheTable;

    @ElementTitle("CSV")
    @FindBy(xpath = "//*[text()='.CSV']/ancestor::button")
    public WebElement btnCSV;

    @ElementTitle("CSV.gz")
    @FindBy(xpath = "//*[text()='.CSV.gz']/ancestor::button")
    public WebElement btnCSVgz;

    @ElementTitle("JSON")
    @FindBy(xpath = "//*[text()='.JSON']/ancestor::button")
    public WebElement btnJSON;

    @ElementTitle("JSON.gz")
    @FindBy(xpath = "//*[text()='.JSON.gz']/ancestor::button")
    public WebElement btnJSONgz;

    @ElementTitle("XML")
    @FindBy(xpath = "//*[text()='.XML']/ancestor::button")
    public WebElement btnXML;

    @ElementTitle("XML.gz")
    @FindBy(xpath = "//*[text()='.XML.gz']/ancestor::button")
    public WebElement btnXMLgz;

    @FindBy(xpath = "//div[@id='eventsCaptureOverlay'] | //div[contains(@class, 'spinner')]")
    public WebElement eventsCaptureOverlay;

    private List<String> correctTimes = new ArrayList<>();

    @ActionTitle("проверяет количество событий в таблице")
    public void checkCountOfEventsOfTable(String count) {
        DriverExtension.waitUntilElementGetInvisible(eventsCaptureOverlay);
        Assert.assertEquals("Количество событий в таблице не равно ожидаемому", count, String.valueOf(Tables.getRows(table).size()));
    }

    @ActionTitle("запоминает время событий в таблице на данной странице")
    public void remembersTheTimeOfEventsInTheTableOnThisPage() {
        DriverExtension.waitUntilElementGetInvisible(eventsCaptureOverlay);
        String formatTimeOfEvent = "\\d{2}\\.\\d{2}\\.\\d{4} \\d{2}:\\d{2}:\\d{2} \\d{3}";
        for (WebElement elementOfEvent : Tables.getColumnByHeading(table, "Время события")) {
            String timeOfEvent = elementOfEvent.getText();
            Assert.assertTrue("Некорректный формат времени события: " + timeOfEvent, timeOfEvent.matches(formatTimeOfEvent));
            toAllure("Найдено событие на данной странице, время", timeOfEvent);
            correctTimes.add(timeOfEvent);
        }
    }

    @ActionTitle("заполняет часовой пояс значением")
    public void fillTheTimeZone(String value) throws PageException {
        WebElement timeZone = PageFactory.getInstance().getCurrentPage().getElementByTitle("Параметры отображения часовой пояс");
        Tools.clearElement(timeZone);
        timeZone.sendKeys(value);
        timeZone.sendKeys(Keys.TAB);
        //TimeZones.selectByIndex(timeZone, 0);
        toAllure("Часовой пояс заполнен значением", value);
        toAllure("Из выпадающего списка выбрано", "первое значение");
    }

    @ActionTitle("проверяет изменение часового пояса в таблице на данной странице")
    public void checkTheTimeZoneChangeInTheTableOnThisPage(String changesInTimeZone) {
        String formatTimeOfEvent = "\\d{2}\\.\\d{2}\\.\\d{4} \\d{2}:\\d{2}:\\d{2} \\d{3}";
        String timeOfEvent;
        List<WebElement> colOfTimeZone = Tables.getColumnByHeading(table, "Время события");
        for (int i = 0; i < colOfTimeZone.size(); i++) {
            timeOfEvent = colOfTimeZone.get(i).getText();
            Assert.assertTrue("Некорректный формат времени события: " + timeOfEvent,
                    timeOfEvent.matches(formatTimeOfEvent));
            Calendar previousHour = new GregorianCalendar();
            Calendar currentHour = new GregorianCalendar();
            previousHour.set(Calendar.HOUR_OF_DAY, Integer.parseInt(correctTimes.get(i).split(" ")[1].split(":")[0]));
            currentHour.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeOfEvent.split(" ")[1].split(":")[0]));
            Assert.assertEquals("Часовой пояс не соответствует ожидаемому, индекс строки: " + i,
                    previousHour.get((Calendar.HOUR_OF_DAY)) + Integer.parseInt(changesInTimeZone),
                    currentHour.get((Calendar.HOUR_OF_DAY)));
            toAllure("Время в часах на строке " + i + " до изменения", String.valueOf(previousHour.get((Calendar.HOUR_OF_DAY))));
            toAllure("Время в часах на строке " + i + " после изменения на " + changesInTimeZone, String.valueOf(currentHour.get((Calendar.HOUR_OF_DAY))));
        }
    }

    @ActionTitle("сверяет данные заголовков таблицы с настройками отображения")
    public void comparesTableHeadingWithDisplaySettings() {
        List<String> labelsOfDisplaySettings = Stash.getValue("labelsOfDisplaySettings");
        toAllure("Берём labels из Stash по ключу", "labelsOfDisplaySettings");
        Tables.checkLables(table, labelsOfDisplaySettings);
    }

    @ActionTitle("проверяет работу сортировки по заголовку")
    public void checkTheWorkOfSortingByTitle(String heading, String descOrAsc) throws PageException {
        DriverExtension.waitUntilElementGetInvisible(eventsCaptureOverlay);
        List<String> labelsOfDisplaySettings = Stash.getValue("labelsOfDisplaySettings");
        toAllure("Берём labels из Stash по ключу", "labelsOfDisplaySettings");
        if (!labelsOfDisplaySettings.contains(heading)) {
            throw new AutotestError("Указанный заголовок + \"" + heading + "\" не найден в списке допустимых: " + labelsOfDisplaySettings);
        }
        toAllure("Заголовок", heading);
        String xpathToSort;
        switch (descOrAsc) {
            case "по возрастанию":
            case "по возрастанию времени входа в приложение":
                xpathToSort = Constants.XPATH_TO_SORT_ASC;
                break;
            case "по убыванию":
                xpathToSort = Constants.XPATH_TO_SORT_DESC;
                break;
            default:
                throw new AutotestError("Некорректное значение для сортировки");
        }
        while (PageFactory.getWebDriver().findElements(By.xpath(xpathToSort))
                .size() == 0 || !PageFactory.getWebDriver().findElement(By.xpath(xpathToSort)).getText().equals(heading)) {
            Tables.getHeadings(table).stream().filter(element -> heading.equals(element.getText())).forEach(WebElement::click);
            toAllure("Выбираем нужную сортировку нажатием по заголовку", heading);
            DriverExtension.waitUntilElementGetInvisible(eventsCaptureOverlay);
        }
        toAllure("У заголовка \"" + heading + "\" успешно выбрана сортировка", descOrAsc);
        List<String> actualColumn = Tables.getColumnByHeading(table, heading).stream().map(element -> element.getText().trim()).collect(toList());
        toAllure("Актуальные значения колонки у заголовка \"" + heading + "\"", String.valueOf(actualColumn));
        List<String> sortedColumn = new ArrayList<>(actualColumn);
        Collections.sort(sortedColumn, String::compareTo);
        switch (descOrAsc) {
            case "по возрастанию":
                toAllure("Отсортированные значения колонки по возрастанию у заголовка \"" + heading + "\"", String.valueOf(actualColumn));
                break;
            case "по возрастанию времени входа в приложение":
                String currentTime = Stash.getValue("currentTime");
                Assert.assertNotNull("Отсутствует значение в Stash по ключу \"currentTime\"", currentTime);
                String[] splitCurrentTime = currentTime.split(":");
                currentTime = splitCurrentTime[0] + ":" + splitCurrentTime[1];
                String formatTimeOfEvent = "\\d{2}\\.\\d{2}\\.\\d{4} \\d{2}:\\d{2}:\\d{2} \\d{3}";
                String firstActTimeOfEvent = actualColumn.get(0);
                Assert.assertTrue("Некорректный формат времени события: " + firstActTimeOfEvent,
                        firstActTimeOfEvent.matches(formatTimeOfEvent));
                String[] splitFirstActualText = firstActTimeOfEvent.split(" ")[1].split(":");
                String firstActualTime = splitFirstActualText[0] + ":" + splitFirstActualText[1];
                Assert.assertEquals("Записи журнала не начинаются со времени входа в приложение", currentTime, firstActualTime);
                toAllure("Записи журнала начинаются со времени входа в приложение", String.valueOf(actualColumn));
                break;
            case "по убыванию":
                Collections.reverse(sortedColumn);
                toAllure("Отсортированные значения колонки по убыванию у заголовка \"" + heading + "\"", String.valueOf(sortedColumn));
                break;
            default:
                throw new AutotestError("Некорректное значение для сортировки");
        }
        Assert.assertEquals("Некорректная сортировка по заголовку \"" + heading + "\"", sortedColumn, actualColumn);
    }

    @ActionTitle("проверяет значения заголовка на допустимые")
    public void checkTheValuesWithHeadingForValidValues(String heading, DataTable dataTable) throws PageException {
        List<String> validValues = dataTable.asList(String.class).stream().map(Tools::mapToStashedValueIfPossible).collect(toList());
        DriverExtension.waitUntilElementGetInvisible(eventsCaptureOverlay);
        List<String> actualColumn = Tables.getColumnByHeading(table, heading).stream().map(element -> element.getText().trim()).collect(toList());
        toAllure("Допустимые значения у заголовка \"" + heading + "\"", String.valueOf(validValues));
        toAllure("Актуальные значения у заголовка \"" + heading + "\"", String.valueOf(actualColumn));
        for (String actualValue : actualColumn) {
            Assert.assertTrue("Недопустимое значение \"" + actualValue + "\" у заголовка \"" + heading + "\"," +
                    " допустимые: " + String.valueOf(validValues), validValues.contains(actualValue));
        }
    }

    @ActionTitle("запоминает значения первой строки столбца")
    public void rememerCellValue(String columnName, String varName) {
        DriverExtension.waitUntilElementGetInvisible(eventsCaptureOverlay);
        List<WebElement> column = Tables.getColumnByHeading(table, columnName);
        Assert.assertTrue("В таблице не найдено ни одной записи", column != null && !column.isEmpty());
        if (!column.isEmpty()) {
            Stash.put(varName, column.get(0).getText());
        }
    }

    @ActionTitle("проверяет все значения столбца на заданное значение")
    public void checkCellValues(String columnName, String varName) {
        DriverExtension.waitUntilElementGetInvisible(eventsCaptureOverlay);
        List<WebElement> column = Tables.getColumnByHeading(table, Tools.mapToStashedValueIfPossible(columnName));
        Assert.assertTrue("В таблице не найдено ни одной записи", column != null && !column.isEmpty());
        if (!column.isEmpty()) {
            for(int i = 0; i < Tables.getRows(table).size(); i++){

                Assert.assertEquals("Результат поиска не удовлетворяет заданным условиям", Tools.mapToStashedValueIfPossible(varName), column.get(i).getText());
            }
        }
    }

    @ActionTitle("проверяет на корректность идентификатор стенда")
    public void checkStandIdCellValues(String columnName, String varName) {
        DriverExtension.waitUntilElementGetInvisible(eventsCaptureOverlay);
        List<WebElement> column = Tables.getColumnByHeading(table, columnName);
        Assert.assertTrue("В таблице не найдено ни одной записи", column != null && !column.isEmpty());
        if (!column.isEmpty()) {
            for(int i = 0; i < Tables.getRows(table).size(); i++){
                Assert.assertEquals("Результат поиска не удовлетворяет заданным условиям", Stash.getValue(varName), column.get(i).getText());
            }
        }
    }

    @ActionTitle("запоминает время создания первой записи")
    public void rememberFirstRowTime() {
        DriverExtension.waitUntilElementGetInvisible(eventsCaptureOverlay);
        List<WebElement> column = Tables.getColumnByHeading(table, "Время события");
        Assert.assertTrue("В таблице ен найдено ни одной записи", column != null && !column.isEmpty());
        if (!column.isEmpty()) {
            Stash.put("время_события", column.get(0).getText().split(" ")[1]);
        }
    }

    @ActionTitle("проверяет текущее время со сдвигом часов у TextBox на эквивалентность")
    public void checkTimeOnElementInSpecialFormat(String elementTitle, String shiftOfHour) throws PageException {
        String formatTime = "\\d{1,2}:\\d{2}:\\d{2}";
        String toFixFormatTime = "0\\d";
        Calendar cal = Stash.getValue("currentTime");
        toAllure("Берём текущее время из Stash по ключу \"currentTime\"", cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE));
        Assert.assertNotNull("Отсутствует значение в Stash по ключу \"currentTime\"", cal);
        cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(shiftOfHour));
        String currentTimeOneHourBack = cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE);
        WebElement element = PageFactory.getInstance().getCurrentPage().getElementByTitle(elementTitle);
        String actualText = Tools.getValue(element);
        Assert.assertTrue("Некорректный формат времени: " + actualText,
                actualText.matches(formatTime));
        String[] splitActualText = actualText.split(":");
        String actualHours = splitActualText[0];
        String actualMinutes = splitActualText[1];
        Calendar actualCal = Calendar.getInstance();
        actualCal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(actualHours));
        actualCal.set(Calendar.MINUTE, Integer.parseInt(actualMinutes));
        actualHours = actualHours.matches(toFixFormatTime) ? actualHours.substring(1) : actualHours;
        actualMinutes = actualMinutes.matches(toFixFormatTime) ? actualMinutes.substring(1) : actualMinutes;
        String actualTextWithFix = actualHours + ":" + actualMinutes;
        assertTrue("Текущее время со сдвигом часов на " + shiftOfHour + ": " + actualTextWithFix + " не соответствует " +
                "ожидаемому: " + currentTimeOneHourBack + " +-2 мин", (cal.getTimeInMillis() - actualCal.getTimeInMillis()) < 120000);
        toAllure("Текущее время со сдвигом часов на " + shiftOfHour, currentTimeOneHourBack);
        toAllure("Время в элементе " + elementTitle + " со сдвигом часов на " + shiftOfHour, actualTextWithFix);
        toAllure("Записываем актуальное время входа в приложение в Stash по ключу \"currentTime\"", actualText);
        Stash.put("currentTime", actualText);
    }

    @ActionTitle("проверяет наличие значений над полем MultiComboBox")
    public void checkTheAvailabilityComboListTags(String multiComboBoxTitle, DataTable dataTable) throws PageException {
        WebElement multiComboBox = getElementByTitle(multiComboBoxTitle);
        List<String> expectedValues = dataTable.asList(String.class).stream().map(v -> Stash.getValue(v) != null ? Stash.getValue(v) : v).collect(toList());
        List<WebElement> actualTagsItem = MultiComboBoxes.getComboTagsList(multiComboBox);
        Assert.assertEquals("Количество значений над полем \"" + multiComboBoxTitle + "\" не соответствует ожидаемому",
                expectedValues.size(), actualTagsItem.size());
        toAllure("Количество значений над полем: ", String.valueOf(actualTagsItem.size()));
        for (WebElement tagsItem : actualTagsItem) {
            String textOfTagsItem = Tools.getValue(tagsItem);
            Assert.assertTrue("Над полем \"" + multiComboBoxTitle + "\" не содержится значение \"" + textOfTagsItem +
                    "\"", expectedValues.contains(textOfTagsItem));
        }
        toAllure("Значения над полем: \"" + multiComboBoxTitle + "\"", String.valueOf(expectedValues));
    }

    private void goToWindow(int index) {
        PageFactory.getWebDriver().switchTo().window(PageFactory.getWebDriver().getWindowHandles().toArray()[index].toString());
    }

    @ActionTitle("ожидает окно XML файла")
    public void awaitingWindowForXMLFile() throws InterruptedException {
        Thread.sleep(5);
        try {
            toAllure("Попытка обнаружения", "вкладка XML");
            goToWindow(1);
            toAllure("Переключение на вкладку XML", "Успешно");
            goToWindow(0);
            toAllure("Переключение на первую вкладку браузера", "Успешно");
        } catch (Exception e) {
            throw new AutotestError("Не открылось окно XML файла", e);
        }
    }

    @ActionTitle("ожидает окно сохранения файла и сохраняет файл")
    public void awaitingWindowForSavingFile(String typeOfFile) throws InterruptedException, IOException {
        if (Files.exists(Paths.get(Constants.PATH_TO_DOWNLOAD_DIR))) {
            Stream<Path> files = Files.list(Paths.get(Constants.PATH_TO_DOWNLOAD_DIR));
            files.forEach(f -> {
                try {
                    Files.deleteIfExists(f);
                } catch (IOException e) {
                    logger.log(Level.INFO, e.getMessage(), e);
                }
            });
        } else {
            Files.createDirectory(Paths.get(Constants.PATH_TO_DOWNLOAD_DIR));
        }
        Thread.sleep(10000);
        StringSelection stringSelectionPath = new StringSelection(Constants.PATH_TO_DOWNLOAD_DIR + "\\");
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelectionPath, null);
        try {
            Robot robot = new Robot();
            robot.setAutoDelay(1000);
            switch (typeOfFile.toLowerCase()) {
                case Constants.CSV:
                case Constants.XML:
                    robot.keyPress(KeyEvent.VK_TAB);
                    robot.keyRelease(KeyEvent.VK_TAB);
                    robot.keyPress(KeyEvent.VK_ENTER);
                    robot.keyRelease(KeyEvent.VK_ENTER);
                    break;
                case Constants.CSV_GZ:
                case Constants.JSON:
                case Constants.JSON_GZ:
                case Constants.XML_GZ:
                    robot.keyPress(KeyEvent.VK_F6);
                    robot.keyRelease(KeyEvent.VK_F6);
                    robot.keyPress(KeyEvent.VK_TAB);
                    robot.keyRelease(KeyEvent.VK_TAB);
                    robot.keyPress(KeyEvent.VK_DOWN);
                    robot.keyRelease(KeyEvent.VK_DOWN);
                    robot.keyPress(KeyEvent.VK_DOWN);
                    robot.keyRelease(KeyEvent.VK_DOWN);
                    robot.keyPress(KeyEvent.VK_ENTER);
                    robot.keyRelease(KeyEvent.VK_ENTER);
                    robot.keyPress(KeyEvent.VK_HOME);
                    robot.keyPress(KeyEvent.VK_CONTROL);
                    robot.keyPress(KeyEvent.VK_V);
                    robot.keyRelease(KeyEvent.VK_V);
                    robot.keyRelease(KeyEvent.VK_CONTROL);
                    robot.keyPress(KeyEvent.VK_ENTER);
                    robot.keyRelease(KeyEvent.VK_ENTER);
                    break;
                default:
                    throw new NotImplementedException();
            }
            File[] files = new File(Constants.PATH_TO_DOWNLOAD_DIR).listFiles();
            long now = (new Date()).getTime();
            while (files != null && Arrays.stream(files).noneMatch(f -> f.getAbsolutePath().toLowerCase().endsWith(typeOfFile.toLowerCase()))) {
                files = new File(Constants.PATH_TO_DOWNLOAD_DIR).listFiles();
                if ((new Date().getTime()) - now > 120000) {
                    fail("Файл не загрузился за 2 минуты");
                }
            }
        } catch (AWTException e) {
            throw new AutotestError("Проблема с окном сохранения файла");
        }
    }

    private List<File> sortedFilesInDirectory(String directory, String typeOfFile) {
        File downloadDirectory = new File(directory);
        List<File> listFiles = Arrays.stream(downloadDirectory.listFiles()).filter(f -> f.getName().toLowerCase().endsWith("." + typeOfFile)).sorted(Comparator.comparingLong(File::lastModified).reversed()).collect(Collectors.toList());
        Assert.assertNotNull("Не найдено ни одного файла в директории " + directory, listFiles);
        return listFiles;
    }

    @ActionTitle("проверяет количество событий в файле")
    public void checksTheAmountOfEventsInTheFile(String typeOfFile) throws IOException {
        DriverExtension.waitUntilElementPresent(eventsFoundInTheTable);
        String amountOfEventsInTheTable = eventsFoundInTheTable.getText().replaceAll(" ", "");
        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher(amountOfEventsInTheTable);
        amountOfEventsInTheTable = matcher.find() ? matcher.group() : null;
        Assert.assertNotNull("Количество событий не определено", amountOfEventsInTheTable);
        Assert.assertNotEquals("Не найдено ни одного события в таблице", "0", amountOfEventsInTheTable);
        toAllure("Количество событий на стенде", amountOfEventsInTheTable);


        String amountOfEventsOfFile = "";

        // xml-файл не скачивается, а открывается в новой вкладке IE
        if (typeOfFile.equalsIgnoreCase(Constants.XML)) {
            goToWindow(1);
            toAllure("Переключение на вкладку XML", "Успешно");
            amountOfEventsOfFile = String.valueOf(((JavascriptExecutor) PageFactory.getWebDriver()).executeScript("return document.getElementsByTagName('doc').length"));
            goToWindow(0);
            toAllure("Переключение на первую вкладку браузера", "Успешно");
            Assert.assertTrue("Количество событий в файле превышает максимальное значение (>10000)", 10000 >= Integer.valueOf(amountOfEventsOfFile));
            return;
        }

        List<File> files = sortedFilesInDirectory(Constants.PATH_TO_DOWNLOAD_DIR, typeOfFile.toLowerCase());
        if (files != null && !files.isEmpty()) {
            List<String> fileContent = Tools.getFileContent(files.get(0).getAbsolutePath());
            toAllure("Найден файл", files.get(0).getAbsolutePath());
            switch (typeOfFile.toLowerCase()) {
                case Constants.CSV:
                case Constants.CSV_GZ:
                    amountOfEventsOfFile = String.valueOf(fileContent.size() - 1);
                    break;
                case Constants.JSON:
                case Constants.JSON_GZ:
                case Constants.XML_GZ:
                    amountOfEventsOfFile = String.valueOf(fileContent.size());
                    break;
                case Constants.XML:
                    break;
                default:
                    throw new AutotestError("Неопределённый тип файла: " + typeOfFile);
            }
        } else {
            throw new AutotestError("Не найден файл нужного расширения в директории " + Constants.PATH_TO_DOWNLOAD_DIR);
        }

        Assert.assertNotEquals("События в файле отсутствуют", "0", amountOfEventsOfFile);
        toAllure("Количество событий в файле", amountOfEventsOfFile);
    }

    @ActionTitle("проверяет наличие значений из таблицы на данной странице в файле")
    public void checksTheValuesOfTheTableInTheFile(String typeOfFile) throws IOException {
        DriverExtension.waitUntilElementGetInvisible(eventsCaptureOverlay);
        boolean isFindFile = false;
        List<String> actualCells = new ArrayList<>();
        List<String> isContainsCells = new ArrayList<>();
        List<String> isNotContainsCells = new ArrayList<>();
        List<WebElement> headingsOfTable = Tables.getHeadings(table);
        List<List<String>> rowsOfTable = Tables.getRowsAsTextButEmptyAndDate(table);
        List<String> fileContent;

        for (File file : sortedFilesInDirectory(Constants.PATH_TO_DOWNLOAD_DIR, typeOfFile.toLowerCase())) {
            switch (typeOfFile.toLowerCase()) {
                case Constants.CSV:
                case Constants.CSV_GZ:
                case Constants.JSON:
                case Constants.JSON_GZ:
                case Constants.XML_GZ:
                    toAllure("Найден файл", file.getAbsolutePath());
                    isFindFile = true;
                    fileContent = Tools.getFileContent(file.getAbsolutePath());
                    toAllure("Фильтры поиск", "значений из таблицы в файле");
                    for (String fileRow : fileContent) {
                        for (int i = 0; i < rowsOfTable.size(); i++) {
                            if (Tools.compareRow(fileRow, rowsOfTable.get(i))) {
                                rowsOfTable.remove(i);
                                break;
                            }
                        }
                        if (rowsOfTable.isEmpty()) {
                            break;
                        }
                    }
                    toAllure("Попытка удаления файла " + file.getName() + " по окончанию работ", String.valueOf(file.delete()));
                    Assert.assertTrue("Данные значения из таблицы не найдены в файле: " + rowsOfTable, rowsOfTable.isEmpty());
                    break;
                case Constants.XML:
                    isFindFile = true;
                    break;
            }
            if (isFindFile) {
                break;
            }
        }
        // xml-файл не скачивается, а открывается в новой вкладке IE
        if (typeOfFile.equalsIgnoreCase(Constants.XML)) {
            goToWindow(1);
            toAllure("Переключение на вкладку XML", "Успешно");
            String xmlSource = PageFactory.getWebDriver().getPageSource();
            // поиск на contains элементов в DOM
            goToWindow(0);
            toAllure("Переключение на первую вкладку браузера", "Успешно");
            for (int i = 0; i < headingsOfTable.size(); i++) {
                for (int j = 1; j <= rowsOfTable.size(); j++) {
                    String textOfCellInTable = Tables.getColumnByIndex(table, i + 1).get(j).getText();
                    actualCells.add(textOfCellInTable);
                    if (textOfCellInTable.matches("\\d{2}\\.\\d{2}\\.\\d{4} \\d{2}:\\d{2}:\\d{2} \\d{3}")) {
                        // приводим дату формата 03.11.2017 14:42:48 744 к формату 2017-11-03T11:42:54
                        // для проверки наличия дат в файле в связи с форматированием ISO
                        String[] splitDate = textOfCellInTable.split(" ");
                        textOfCellInTable = splitDate[0].split("\\.")[2] + "-" + splitDate[0].split("\\.")[1] + "-" + splitDate[0].split("\\.")[0] + "T" + splitDate[1];
                    }
                    if (xmlSource.contains(textOfCellInTable)) {
                        isContainsCells.add(textOfCellInTable);
                    } else {
                        isNotContainsCells.add(textOfCellInTable);
                    }
                }
            }
        }
        if (!isFindFile) {
            throw new AutotestError("Возможна проблема со скачиванием файла. Не найден файл нужного расширения в директории: " + Constants.PATH_TO_DOWNLOAD_DIR);
        }
        toAllure("Количество ненайденных значений из таблицы в файле", String.valueOf(rowsOfTable.size()));

    }

    @ActionTitle("нажимает на кнопку, ожидая оверлей")
    public void clickOnElementAndWaintingOverlay(String nameElement) {
        DriverExtension.waitUntilElementGetInvisible(eventsCaptureOverlay);
        toAllure("нажимает на элемент", nameElement);
        try {
            WebElement element = PageFactory.getInstance().getCurrentPage().getElementByTitle(nameElement);
            Actions actions = new Actions(PageFactory.getWebDriver());
            actions.click(element).build().perform();
        } catch (NoSuchElementException | PageException ex) {
            throw new AutotestError(ex);
        }
        //DriverExtension.waitUntilElementGetInvisible(eventsCaptureOverlay);
    }

    @ActionTitle("получает идентификатор стенда")
    public void getStandId() {
        DriverExtension.waitUntilElementGetInvisible(eventsCaptureOverlay);
        Stash.put("стенд_id", Constants.STAND_ID);
        Stash.getValue("стенд_id");
    }

    @ActionTitle("получает данные для фильтрации")
    public void getTestDataFromXlsx(String sheet, String rowNumber) throws Exception{
        DriverExtension.waitUntilElementGetInvisible(eventsCaptureOverlay);
        String fieldName = new ExcelUtil().dataFromXslx(sheet+Constants.STAND_ID, rowNumber, String.valueOf(0));
        Stash.put("наименование_поля", "Фильтры "+fieldName);
        String fieldValue = new ExcelUtil().dataFromXslx(sheet+Constants.STAND_ID, rowNumber, String.valueOf(1));
        Stash.put("значение_поля", fieldValue);
        Stash.put("столбец", fieldName);
    }

    @ActionTitle("проверяет все значения столбца на заданный промежуток времени")
    public void checkCellValuesTime(String columnName) throws Exception{
        DriverExtension.waitUntilElementGetInvisible(eventsCaptureOverlay);
        List<WebElement> column = Tables.getColumnByHeading(table, Tools.mapToStashedValueIfPossible(columnName));
        Assert.assertTrue("В таблице не найдено ни одной записи", column != null && !column.isEmpty());
        if (!column.isEmpty()) {
            WebElement beginDate = PageFactory.getInstance().getCurrentPage().getElementByTitle("Фильтры Начальная дата");
            WebElement beginTime = PageFactory.getInstance().getCurrentPage().getElementByTitle("Фильтры Начальное время");
            WebElement endDate = PageFactory.getInstance().getCurrentPage().getElementByTitle("Фильтры Конечная дата");
            WebElement endTime = PageFactory.getInstance().getCurrentPage().getElementByTitle("Фильтры Конечное время");
            for(int i = 0; i < Tables.getRows(table).size(); i++){
                Date begDate, enDate, tableDate;
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.YYYY hh.mm.ss");
                begDate = dateFormat.parse(beginDate.getText()+" "+beginTime.getText());
                enDate = dateFormat.parse(endDate.getText()+" "+endTime.getText());
                tableDate = dateFormat.parse(column.get(i).getText());
                //Assert.assertEquals("Результат поиска не удовлетворяет заданным условиям", Tools.mapToStashedValueIfPossible(varName), column.get(i).getText());
            }
        }
    }
}
