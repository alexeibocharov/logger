package ru.sbt.qa.utabs.otpprb.logger.helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MultiComboBoxes {

    public static List<WebElement> getComboTagsList(WebElement multiComboBox) {
        List<WebElement> elementsList = getComboTags(multiComboBox).findElements(By.xpath("./div"));
        List<WebElement> tagsList = new ArrayList();
        Iterator var3 = elementsList.iterator();

        while (var3.hasNext()) {
            WebElement element = (WebElement) var3.next();
            WebElement tagsItem = element;
            //tagsItem.setName(element.getText());
            //TODO: make this work
            tagsList.add(tagsItem);
        }

        return tagsList;
    }

    private static WebElement getComboTags(WebElement element) throws NoSuchElementException {
        return element.findElement(By.xpath(".//div[contains(@class, 'input__list')]"));
    }
}
