package ru.sbt.qa.utabs.otpprb.logger;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import ru.sbtqa.tag.cucumber.TagCucumber;

/**
 *
 * @author sbt-ulyanov
 */
@RunWith(TagCucumber.class)
@CucumberOptions(
        monochrome = true,
        plugin = {"pretty"},
        glue = {"ru.sbt.qa.utabs.otpprb.logger.stepDefs", "ru.sbtqa.tag.pagefactory.stepdefs"},
        //tags = {"@8836, @8062"},
        features = {"src/test/resources/features/"}
)
public class CucumberTest {
}
