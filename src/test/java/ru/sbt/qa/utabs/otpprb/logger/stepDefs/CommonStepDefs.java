package ru.sbt.qa.utabs.otpprb.logger.stepDefs;

import cucumber.api.java.Before;
import cucumber.api.java.ru.Дано;
import cucumber.api.java.ru.И;
import cucumber.api.java.ru.Когда;
import cucumber.api.java.ru.Тогда;
import org.junit.Assert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbt.qa.utabs.otpprb.logger.helpers.Constants;
import ru.sbt.qa.utabs.otpprb.logger.helpers.Kaffka;
import ru.sbt.qa.utabs.otpprb.logger.helpers.Tools;
import ru.sbt.qa.utabs.otpprb.logger.pages.AuthorizationPage;
import ru.sbt.qa.utabs.otpprb.logger.pages.MainPage;
import ru.sbtqa.tag.allurehelper.ParamsHelper;
import ru.sbtqa.tag.datajack.Stash;
import ru.sbtqa.tag.pagefactory.Page;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.drivers.TagWebDriver;
import ru.sbtqa.tag.qautils.errors.AutotestError;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;

import static ru.sbt.qa.utabs.otpprb.logger.utils.AllureHelper.toAllure;

/**
 * @author sbt-kiyashenko-io
 */
public class CommonStepDefs {

    private static final Logger LOG = LoggerFactory.getLogger(Page.class);

    @Before
    public void StartUp() {
        File[] tmpFiles = (new File(Constants.PATH_TO_DOWNLOAD_DIR).listFiles());
        if (tmpFiles != null) {
            for (File f : tmpFiles) {
                Tools.deleteDirectory(f);
            }
        }
    }

    @Дано("^пользователь ждет \"([^\"]*)\" секунд$")
    public void staticWait(String time) throws InterruptedException {
        // Write code here that turns the phrase above into concrete actions
        Thread.sleep(Integer.parseInt(time) * 1000);
    }

    //проверка наличия блоков на главной странице
    public void blocksExist(MainPage page){
        page.blockExists("Конфигурация журналирования");
        page.blockExists("Системный журнал ППРБ");
        page.blockExists("Журнал интеграционного модуля ППРБ");
        page.blockExists("Журнал событий КБТ");
    }

    @И("^пользователь авторизуется в системе$")
    public void logIn() throws Exception {
        AuthorizationPage authPage = (AuthorizationPage) PageFactory.getInstance().getPage(AuthorizationPage.class);
        if(PageFactory.getWebDriver().getCurrentUrl().equals(TagWebDriver.WEBDRIVER_SUDIR_URL) && !TagWebDriver.WEBDRIVER_SUDIR_URL.isEmpty()){
            try{
                authPage.loginSudir();
            }catch (TimeoutException ex){
                MainPage page = (MainPage) PageFactory.getInstance().getPage(MainPage.class);
                try{
                    blocksExist(page);
                }catch (Exception ex1){
                    LOG.debug("Авторизация через СУДИР не сработала, осуществляется перенаправление на прямой URL " + TagWebDriver.WEBDRIVER_DIRECT_URL);
                    PageFactory.getWebDriver().get(TagWebDriver.WEBDRIVER_DIRECT_URL);
                    try{
                        authPage.loginDirect();
                    }catch (TimeoutException ex2){
                        try{
                            blocksExist(page);
                        }catch (Exception ex3){
                            throw new Exception("После авторизации главная страница загружена некорректно");
                        }
                    }
                }
            }
        }else{
            LOG.debug("Авторизация через СУДИР не сработала, осуществляется перенаправление на прямой URL " + TagWebDriver.WEBDRIVER_DIRECT_URL);
            PageFactory.getWebDriver().get(TagWebDriver.WEBDRIVER_DIRECT_URL);
            try{
                authPage.loginDirect();
            }catch(TimeoutException ex){
                MainPage page = (MainPage) PageFactory.getInstance().getPage(MainPage.class);
                try{
                    blocksExist(page);
                }catch (Exception ex2){
                    throw new Exception("После авторизации главная страница загружена некорректно");
                }
            }
        }
    }

    @Тогда("пользователь посылает сообщение из файла \"(.*?)\" в топик с именем \"(.*?)\"$")
    public void messageToKaffka(String templateName, String topic){
        Kaffka kaffka = new Kaffka();
        kaffka.SendToKaffka(topic,templateName);
    }


    @И("^пользователь запоминает текущее время$")
    public void rememberCurrentTime() {
        Calendar cal = Calendar.getInstance();
        toAllure("Записываем текущее время в Stash по ключу \"currentTime\"", cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE));
        Stash.put("currentTime", cal);
    }

    @Тогда("^(?:пользователь |он |)на элементе \"(.*?)\" сверяет текст на эквивалентность \"(.*?)\"")
    public void checkText(String nameElement, String text) throws Exception {
        text = GenerateRandTestData.checkTestData(null, text);
        ParamsHelper.addParam(nameElement, text);
        WebElement element = PageFactory.getInstance().getCurrentPage().getElementByTitle(nameElement);
        String foundText = Tools.getValue(element);
        Assert.assertTrue("Тексты не равны: " + foundText + " != " + text, foundText.equals(text));
    }

    @Когда("^(?:пользователь |он |)проверяет наличие (?:псевдо-ссылки |чекбокса |кнопки |элемента |поля | )\"(.*)\"(?: на странице|)")
    @И("^элемент \"(.*)\" присутствует на странице")
    public void elementAlwaysPresent(String elementTitle) {
        try {
            StepDefs.switchTab(elementTitle);
            Assert.assertTrue("Элемент не найден: " + elementTitle, isElementPresent(elementTitle));
        } catch (Exception ex) {
            throw new AutotestError(ex);
        }
    }

    private static WebDriver InitDriver() {
        //System.setProperty("webdriver.chrome.driver", "src/test/resources/webdrivers/IEDriverServer.exe");
        String downloadPath = "src\\test\\resources\\tmp";

        HashMap<String, Object> chromePrefs = new HashMap<>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.default_directory", downloadPath);
        chromePrefs.put("profile.password_manager_enabled", false);
        chromePrefs.put("credentials_enable_service", false);


        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePrefs);
        options.addArguments("--start-maximized");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);

        return new ChromeDriver(capabilities);
    }

    public boolean isElementPresent(String elementTitle) throws Exception {
        try {
            WebElement typifiedElement = PageFactory.getInstance().getCurrentPage().getElementByTitle(elementTitle);
            return this.isElementPresent(typifiedElement);
        } catch (NoSuchElementException ex) {
            return false;
        }
    }

    public boolean isElementPresent(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (NoSuchElementException ex) {
            return false;
        }
    }
}
