package ru.sbt.qa.utabs.otpprb.logger.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.sbtqa.tag.pagefactory.Page;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.extensions.DriverExtension;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

public class CommonPage extends Page {

    @FindBy(xpath = "//div[@id='eventsCaptureOverlay'] | //div[contains(@class, 'spinner')]")
    public WebElement eventsCaptureOverlay;

    public CommonPage() {
        PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(PageFactory.getWebDriver())), this);
    }

    public void waitForEventCaptureOverlayDisappear() {
        DriverExtension.waitUntilElementGetInvisible(eventsCaptureOverlay);
    }
}
