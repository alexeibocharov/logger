package ru.sbt.qa.utabs.otpprb.logger.pages.systemJournalOfPprb;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.sbt.qa.utabs.otpprb.logger.helpers.Lists;
import ru.sbt.qa.utabs.otpprb.logger.pages.CommonPage;
import ru.sbtqa.tag.datajack.Stash;
import ru.sbtqa.tag.pagefactory.annotations.ActionTitle;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.sbtqa.tag.pagefactory.extensions.DriverExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ru.sbt.qa.utabs.otpprb.logger.utils.AllureHelper.toAllure;

/**
 * Created by sbt-kiyashenko-io on 18.10.2017.
 */
@PageEntry(title = "Настройки отображения колонок")
public class DisplaySettingsModalWindow extends CommonPage {

    @ElementTitle("Настройки отображения")
    @FindBy(xpath = "//div[text()]/ancestor::div[contains(@class, 'field_horizontal')]")
    public List<WebElement> listSelectBoxOfDisplaySettings;

    @ElementTitle("Настройка отображения ID")
    @FindBy(xpath = "//div[text()='ID']/ancestor::div[contains(@class, 'field_horizontal')]")
    public WebElement selectBoxOfDisplaySettingsID;

    @ElementTitle("Настройка отображения Время события")
    @FindBy(xpath = "//div[text()='Время события']/ancestor::div[contains(@class, 'field_horizontal')]")
    public WebElement selectBoxOfDisplaySettingsTimeOfEvent;

    @ElementTitle("Закрыть")
    @FindBy(xpath = "//span[contains(@class,'icon_img-close')]")
    public WebElement btnClose;

    @ElementTitle("Расширить")
    @FindBy(xpath = "//button//span[text()='Расширить']")
    public WebElement btnExpand;

    @ElementTitle("Сжать")
    @FindBy(xpath = "//button//span[text()='Сжать']")
    public WebElement btnCompress;

    @FindBy(xpath = "//div[contains(@class, 'boardbar_border')]//div[text()='Настройки отображения колонок']")
    public WebElement initPage;

    private static final String[] TRUE_SELECT_BOX_OPTIONS = {"скрыть", "не переносить", "обрезать", "переносить"};

    public DisplaySettingsModalWindow() {
        super();
        DriverExtension.waitUntilElementPresent(initPage);
    }

    @ActionTitle("проверяет наличие настроек отображения и наличие состояний для каждого атрибута")
    public void checkToEnableListSelectBoxOfDisplaySettings() {
        Assert.assertFalse("Не найдено ни одного атрибута для настройки отображения", listSelectBoxOfDisplaySettings.isEmpty());
        for (WebElement displaySetting : listSelectBoxOfDisplaySettings) {
            List<String> settingsValues = Lists.getListValues(displaySetting);
            Assert.assertTrue("У атрибута \"" + Lists.getLabel(displaySetting) + "\" не найдены необходимые состояния: " +
                    Arrays.toString(TRUE_SELECT_BOX_OPTIONS), settingsValues
                    .containsAll(Arrays.asList(TRUE_SELECT_BOX_OPTIONS)));
            toAllure("У атрибута \"" + Lists.getLabel(displaySetting) + "\" найдены необходимые состояния",
                    String.valueOf(settingsValues));
        }
    }

    @ActionTitle("запоминает все настройки и выставляет им значение")
    public void setListSelectBoxOfDisplaySettings(String value) {
        List<String> labelsOfSelectBox = new ArrayList<>();
        for (WebElement displaySetting : listSelectBoxOfDisplaySettings) {
            labelsOfSelectBox.add(Lists.getLabel(displaySetting));
            if (!value.equals(Lists.getTextFocused(displaySetting))) {
                Lists.selectByValue(displaySetting, value);
            }
        }
        toAllure("Записываем labels в Stash по ключу", "labelsOfDisplaySettings");
        Stash.put("labelsOfDisplaySettings", labelsOfSelectBox);
    }

    /**
     * Проверка длины атрибутов настроек по оси X слева-направо
     * (при увеличении длины значение X уменьшается)
     */
    @ActionTitle("проверяет работу кнопок расширения и сужения")
    public void checkExpandAndCompressButtons() {
        int beforeSizeOfSettings = 0;
        int actualSizeOfSettings;
        for (WebElement displaySetting : listSelectBoxOfDisplaySettings) {
            Assert.assertTrue("Длины полей выбора значения различны", beforeSizeOfSettings == 0 || beforeSizeOfSettings == displaySetting.getLocation().getX());
            beforeSizeOfSettings = displaySetting.getLocation().getX();
        }
        toAllure("Длина поля выбора до расширения", String.valueOf(beforeSizeOfSettings));
        btnExpand.click();
        for (int i = 0; i < listSelectBoxOfDisplaySettings.size(); i++) {
            actualSizeOfSettings = listSelectBoxOfDisplaySettings.get(i).getLocation().getX();
            Assert.assertTrue("Длина атрибута настройки \"" + Lists.getLabel(listSelectBoxOfDisplaySettings.get(i)) + "\" " +
                    "не увеличилась", beforeSizeOfSettings > actualSizeOfSettings);
            if (i == listSelectBoxOfDisplaySettings.size() - 1) {
                beforeSizeOfSettings = actualSizeOfSettings;
                toAllure("Длина поля выбора после расширения", String.valueOf(beforeSizeOfSettings));
                break;
            }
        }
        btnCompress.click();
        for (int i = 0; i < listSelectBoxOfDisplaySettings.size(); i++) {
            actualSizeOfSettings = listSelectBoxOfDisplaySettings.get(i).getLocation().getX();
            Assert.assertTrue("Длина атрибута настройки \"" + Lists.getLabel(listSelectBoxOfDisplaySettings.get(i)) + "\" " +
                    "не уменьшилась", beforeSizeOfSettings < actualSizeOfSettings);
            if (i == listSelectBoxOfDisplaySettings.size() - 1) {
                toAllure("Длина поля выбора после сужения", String.valueOf(actualSizeOfSettings));
                break;
            }
        }
    }
}
