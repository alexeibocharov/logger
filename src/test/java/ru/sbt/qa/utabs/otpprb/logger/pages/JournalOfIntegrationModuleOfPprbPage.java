package ru.sbt.qa.utabs.otpprb.logger.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.sbtqa.tag.pagefactory.extensions.DriverExtension;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

/**
 * Created by sbt-kiyashenko-io on 18.08.2017.
 */
@PageEntry(title = "Журнал интеграционного модуля ППРБ")
public class JournalOfIntegrationModuleOfPprbPage extends JournalPage {

    @FindBy(xpath = "//div[contains(@class, 'toolbar')]/*[text()='Журнал интеграционного модуля ППРБ']")
    public WebElement initPage;

    public JournalOfIntegrationModuleOfPprbPage() {
        PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(PageFactory.getWebDriver())), this);
        DriverExtension.waitUntilElementPresent(initPage);
        DriverExtension.waitUntilElementToBeClickable(btnSearch);
    }
}
