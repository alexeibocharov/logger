package ru.sbt.qa.utabs.otpprb.logger.helpers;

import ru.sbtqa.tag.qautils.properties.Props;

public class Constants {
    public static final String PATH_TO_DOWNLOAD_DIR = Props.get("webdriver.downloadFolder", "C:\\Users\\" + System.getProperty("user.name") + "\\Downloads");
    public static final String XPATH_FILTER_FIELD = "/ancestor::div[contains(@class, 'field') and not(contains(@class, 'horizontal'))]";
    public static final String XPATH_TO_SORT_ASC = "//td[contains(@class, 'table__cell_sorted_asc')]";
    public static final String XPATH_TO_SORT_DESC = "//td[contains(@class, 'table__cell_sorted_desc')]";
    public static final String CSV = "csv";
    public static final String CSV_GZ = "csv.gz";
    public static final String JSON = "json";
    public static final String JSON_GZ = "json.gz";
    public static final String XML = "xml";
    public static final String XML_GZ = "xml.gz";
    public static final String STAND_ID = Props.get("stand.id");
    public static final String TEST_DATA_PATH = Props.get("testDataPath");
}
