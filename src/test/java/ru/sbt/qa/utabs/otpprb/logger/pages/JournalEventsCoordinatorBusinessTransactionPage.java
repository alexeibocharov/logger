package ru.sbt.qa.utabs.otpprb.logger.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.sbtqa.tag.pagefactory.extensions.DriverExtension;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

/**
 * Created by sbt-ryzhkovskiy-vs on 25.04.2018.
 */
@PageEntry(title = "Журнал событий КБТ")
public class JournalEventsCoordinatorBusinessTransactionPage extends JournalPage {

    @FindBy(xpath = "//div[contains(@class, 'toolbar')]/*[text()='Журнал событий КБТ']")
    public WebElement initPage;

    public JournalEventsCoordinatorBusinessTransactionPage() {
        PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(PageFactory.getWebDriver())), this);
        DriverExtension.waitUntilElementPresent(initPage);
        DriverExtension.waitUntilElementToBeClickable(btnSearch);
    }
}
