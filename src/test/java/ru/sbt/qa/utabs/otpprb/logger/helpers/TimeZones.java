package ru.sbt.qa.utabs.otpprb.logger.helpers;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.extensions.DriverExtension;

import java.util.List;

public class TimeZones {

    public static void selectByIndex(WebElement element, int index) {
        List<WebElement> items = getItems(element);
        Assert.assertFalse("No one item is not found", items.isEmpty());
        Assert.assertTrue("Incorrect index \"" + index + "\" size of items = \"" + items.size() + "\"", items.size() > index);
        Actions actions = new Actions(PageFactory.getWebDriver());
        actions.moveToElement((WebElement) items.get(index)).build().perform();
        actions.click((WebElement) items.get(index)).build().perform();
    }

    private static List<WebElement> getItems(WebElement element) {
        DriverExtension.waitUntilElementAppearsInDom(By.xpath("//div[contains(@class, 'dropdown__list-item')]"));
        return element.findElements(By.xpath("//div[contains(@class, 'dropdown__list-item')]"));

    }
}
