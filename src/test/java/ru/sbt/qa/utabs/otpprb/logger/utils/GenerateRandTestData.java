package ru.sbt.qa.utabs.otpprb.logger.utils;

import gens.GeneratorFactory;
import gens.GeneratorInitializationException;

/**
 * Created by sbt-kiyashenko-io on 18.08.2017.
 */
public class GenerateRandTestData {

    protected static String[] getArgs(String args) {
        String[] genArgs = args.split("generate:");
        return genArgs.length > 1 && genArgs[1].split(":").length > 1 ? genArgs[1].split(":")[1].split(" ")[0].split(",") : null;
    }

    public static String checkTestData(String value) throws Exception {
        GeneratorFactory gf = new GeneratorFactory();
        if (value.contains("generate:") && value.split("generate:").length > 1) {
            try {
                value = value.replaceAll("generate:\\S+:\\S+", gf.getGenerator(value.split("generate:")[1].split(":")[0]).generate(getArgs(value)));
            } catch (Exception var4) {
                throw new GeneratorInitializationException("Cant find generator class - " + value.split("generate:")[1], var4);
            }
        }
        return value;
    }
}