package ru.sbt.qa.utabs.otpprb.logger.pages.logLevelSettings;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.sbt.qa.utabs.otpprb.logger.pages.CommonPage;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.sbtqa.tag.pagefactory.extensions.DriverExtension;

/**
 * Created by sbt-kiyashenko-io on 18.08.2017.
 */
@PageEntry(title = "Настройки уровней журналирования")
public class LogLevelSettingsPage extends CommonPage {

    @FindBy(xpath = "//div[contains(@class, 'toolbar')]/*[text()='Настройки уровней журналирования']")
    public WebElement initPage;

    public LogLevelSettingsPage() {
        super();
        DriverExtension.waitUntilElementPresent(initPage);
    }
}