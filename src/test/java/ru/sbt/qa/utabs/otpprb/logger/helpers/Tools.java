package ru.sbt.qa.utabs.otpprb.logger.helpers;

import au.com.bytecode.opencsv.CSVReader;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import ru.sbtqa.tag.datajack.Stash;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;

public class Tools {
    public static String getValue(WebElement element) {
        WebElement input = getInput(element);
        if (input != null) {
            if (input.getTagName().equalsIgnoreCase("input")) {
                return input.getAttribute("value");
            } else {
                return input.getText();
            }
        } else {
            return element.getText();
        }
    }

    public static WebElement getInput(WebElement element) {
        try {
            return element.findElement(By.xpath(".//*[contains(@class,'__value')]"));
        } catch (Exception ex) {
            return null;
        }
    }

    public static String mapToStashedValueIfPossible(String value) {
        String stashedValue = Stash.getValue(value);
        if (stashedValue != null) {
            return stashedValue;
        }
        return value;
    }

    public static void clearElement(WebElement input) {
        input.sendKeys(Keys.chord(Keys.END, Keys.SHIFT, Keys.HOME, Keys.BACK_SPACE));
    }

    public static void deleteDirectory(File file) {
        if (file.exists()) {
            File[] files = file.listFiles();
            if (null != files) {
                for (int i = 0; i < files.length; i++) {
                    if (files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    } else {
                        files[i].delete();
                    }
                }
            }
        }
        file.delete();
    }

    public static List<String> getFileContent(String path) throws IOException {
        BufferedReader bufferedReader;
        if (path.toLowerCase().endsWith("gz")) {
            GZIPInputStream gzipInputStream = new GZIPInputStream(new FileInputStream(path));
            bufferedReader = new BufferedReader(new InputStreamReader(gzipInputStream));
        } else {
            bufferedReader = new BufferedReader(new FileReader(path));
        }
        if (path.toLowerCase().contains(Constants.CSV)) {
            CSVReader csvReader = new CSVReader(bufferedReader);
            List<String[]> fileContent = csvReader.readAll();
            return fileContent.stream().map(r -> Arrays.stream(r).map(Tools::formatValue).collect(Collectors.joining(","))).collect(Collectors.toList());
        }
        if (path.toLowerCase().contains(Constants.JSON)) {
            JsonArray jsonArray = new JsonParser().parse(getFileContentAsString(bufferedReader)).getAsJsonObject().get("response").getAsJsonObject().get("docs").getAsJsonArray();
            List<String> rows = new ArrayList<>();
            for (int i = 0; i < jsonArray.size(); i++) {
                rows.add(formatValue(jsonArray.get(i).toString()));
            }
            return rows;
        }
        if (path.toLowerCase().contains(Constants.XML)) {
            try {
                String fileContent = getFileContentAsString(bufferedReader);
                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                Document doc = documentBuilder.parse(new InputSource(new StringReader(fileContent)));
                doc.getDocumentElement().normalize();
                NodeList docs = doc.getElementsByTagName("doc");
                List<String> docsAsString = new ArrayList<>();
                for (int i = 0; i < docs.getLength(); i++) {
                    docsAsString.add(nodeToString(docs.item(i)));
                }
                return docsAsString;
            } catch (Exception ex) {
                System.out.print(ex.getMessage());
            }
        }
        return null;
    }

    private static String nodeToString(Node node) {
        StringWriter sw = new StringWriter();
        try {
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            t.setOutputProperty(OutputKeys.INDENT, "yes");
            t.transform(new DOMSource(node), new StreamResult(sw));
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        return sw.toString();
    }

    private static String getFileContentAsString(BufferedReader bufferedReader) throws IOException {
        List<String> fileContent = new ArrayList<>();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            fileContent.add(line);
        }
        return fileContent.stream().collect(Collectors.joining());
    }

    public static boolean compareRow(String fileRow, List<String> cellValues) {
        boolean allCellsFound = true;
        List<String> formattedCellValues = cellValues.stream().map(Tools::formatValue).collect(Collectors.toList());
        String formattedFileRow = formatValue(fileRow);
        for (String cellValue : formattedCellValues) {
            if (!formattedFileRow.contains(cellValue)) {
                allCellsFound = false;
                break;
            }
        }
        return allCellsFound;
    }

    public static String formatValue(String text) {
        String formattedValue = text.replace("\r", "").replace("\n", "").replace("\t", "");
        while (formattedValue.contains("  ")) {
            formattedValue = formattedValue.replace("  ", " ");
        }
        return formattedValue;
    }

    public static String formatCellValue(String cellValue) {
        String formattedDate = formatDate(cellValue);
        if (!"".equals(formattedDate)) {
            return formattedDate;
        }
        return cellValue;
    }

    public static boolean isDate(String date) {
        return date.matches("\\d{2}\\.\\d{2}\\.\\d{4} \\d{1,2}:\\d{2}:\\d{2} \\d{3}");
    }

    public static String formatDate(String date) {
        if (isDate(date)) {
            // приводим дату формата 03.11.2017 14:42:48 744 к формату 2017-11-03T11:42:54
            // для проверки наличия дат в файле в связи с форматированием ISO
            String[] splitDate = date.split(" ");
            return splitDate[0].split("\\.")[2] + "-" + splitDate[0].split("\\.")[1] + "-" + splitDate[0].split("\\.")[0] + "T" + splitDate[1];
        }
        return "";
    }
}
