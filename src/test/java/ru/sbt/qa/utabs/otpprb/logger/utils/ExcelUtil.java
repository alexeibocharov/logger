package ru.sbt.qa.utabs.otpprb.logger.utils;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import ru.sbt.qa.utabs.otpprb.logger.helpers.Constants;

import java.io.File;
import java.io.FileInputStream;


/**
 * Helper class to get get data from excel file
 */
public class ExcelUtil {

    public XSSFSheet sheet;
    public XSSFRow row;
    public XSSFCell cell;

    public String dataFromXslx(String sheetName, String rowNum, String cellNum) throws Exception{
        String data = "";
        File file = new File(System.getProperty("user.dir") + Constants.TEST_DATA_PATH);
        FileInputStream inputStream = new FileInputStream(file);
        sheet = new XSSFWorkbook(inputStream).getSheet(sheetName);
        row = sheet.getRow(Integer.parseInt(rowNum));
        cell = row.getCell(Integer.parseInt(cellNum));
        switch(cell.getCellTypeEnum()){
            case NUMERIC:
                data = cell.getRawValue();
            break;
            case STRING:
                data = cell.getStringCellValue();
                break;
        }
        return data;
    }

}
