package ru.sbt.qa.utabs.otpprb.logger.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbtqa.tag.allurehelper.ParamsHelper;

/**
 * Created by sbt-kiyashenko-io on 16.10.2017.
 */
public class AllureHelper {

    private static final Logger LOG = LoggerFactory.getLogger(AllureHelper.class);

    private AllureHelper() {
    }

    public static void toAllure(String message, String value) {
        LOG.info("{} : {}", message, value);
        ParamsHelper.addParam(message.replaceAll("[.:]", " "), value.replaceAll("[.:]", " "));
    }
}