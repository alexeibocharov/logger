package ru.sbt.qa.utabs.otpprb.logger.pages.systemJournalOfPprb;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.sbt.qa.utabs.otpprb.logger.pages.JournalPage;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.sbtqa.tag.pagefactory.extensions.DriverExtension;

/**
 * Created by sbt-kiyashenko-io on 18.08.2017.
 */
@PageEntry(title = "Системный журнал ППРБ")
public class SystemJournalOfPprbPage extends JournalPage {

    @FindBy(xpath = "//div[contains(@class, 'toolbar')]/*[text()='Системный журнал ППРБ']")
    public WebElement initPage;

    @ElementTitle("Таб Фильтры")
    @FindBy(xpath = "//div[contains(@class, 'tabs__item') and contains(text(), 'Фильтры')]")
    public WebElement tabFilters;

    @ElementTitle("Таб Параметры отображения")
    @FindBy(xpath = "//div[contains(@class, 'tabs__item') and contains(text(), 'Параметры отображения')]")
    public WebElement tabViewParams;

    public SystemJournalOfPprbPage() {
        super();
        DriverExtension.waitUntilElementPresent(initPage);
        DriverExtension.waitUntilElementToBeClickable(tabFilters);
        DriverExtension.waitUntilElementToBeClickable(tabViewParams);
    }
}