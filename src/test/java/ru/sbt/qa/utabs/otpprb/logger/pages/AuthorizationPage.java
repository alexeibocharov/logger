package ru.sbt.qa.utabs.otpprb.logger.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import ru.sbtqa.tag.pagefactory.Page;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.annotations.ActionTitle;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.sbtqa.tag.pagefactory.drivers.TagWebDriver;
import ru.sbtqa.tag.pagefactory.exceptions.PageInitializationException;
import ru.sbtqa.tag.pagefactory.extensions.DriverExtension;
import ru.sbtqa.tag.qautils.properties.Props;

import java.util.List;

import static ru.sbtqa.tag.pagefactory.PageFactory.getWebDriver;
import static ru.sbtqa.tag.pagefactory.extensions.WebExtension.waitForPageToLoad;

/**
 * @author sbt-kiyashenko-io
 */
@PageEntry(title = "Страница авторизации")
public class AuthorizationPage extends Page {

    @ElementTitle("Login text input")
    @FindAll({
            @FindBy(xpath = "(//div[contains(@class,'textbox')]//span[contains(@class,'icon_img-profile')])/../..//input"),
            @FindBy(id = "username")
    })
    public WebElement loginInput;

    @ElementTitle("Password text input")
    @FindAll({
            @FindBy(xpath = "(//div[contains(@class,'textbox')]//span[contains(@class,'icon_img-help')])/../..//input"),
            @FindBy(id = "password")
    })
    public WebElement passwordInput;

    @ElementTitle("Entry button")
    @FindAll({
            @FindBy(xpath = "//button//span[text()='Войти']"),
            @FindBy(id = "submitInter")
    })
    public WebElement btnEntry;


    @ElementTitle("Incorrect login or password")
    @FindAll({
            @FindBy(xpath = "//*[contains(text(),'Неверный логин или пароль')]"),
    })
    public WebElement incorrectLogin;

    @ElementTitle("Incorrect login or password Sudir")
    @FindAll({
            @FindBy(xpath = "//*[contains(text(),'Ошибка аутентификации. При входе в систему указаны неправильные имя пользователя и пароль. Для получения доступа к Системе укажите правильные имя пользователя и пароль либо обратитесь к Администратору СУД-ИР.')]"),
            @FindBy(id = "text_error")
    })
    public WebElement incorrectLoginSudir;


    public boolean errorLoginTextExists(WebElement webElement)  {
        DriverExtension.waitUntilElementPresent(webElement);
        return webElement.isDisplayed();
    }

    public AuthorizationPage() {
        try{
            if(!TagWebDriver.WEBDRIVER_SUDIR_URL.isEmpty()){
                PageFactory.getWebDriver().get(TagWebDriver.WEBDRIVER_SUDIR_URL);
                waitForPageToLoad(true);
            }else {
                throw new PageInitializationException("Property 'webdriver.sudir.url' is empty, redirect to 'webdriver.direct.url'");
            }
        }catch (Exception e){
            if(!TagWebDriver.WEBDRIVER_DIRECT_URL.isEmpty()){
                PageFactory.getWebDriver().get(TagWebDriver.WEBDRIVER_DIRECT_URL);
                waitForPageToLoad(true);
            }
        }
        acceptSslWarning();
        PageFactory.initElements(PageFactory.getWebDriver(), this);
        DriverExtension.waitUntilElementPresent(loginInput);
    }

    @ActionTitle("авторизуется с логином и паролем СУДИР")
    public void loginSudir() throws Exception{
        acceptSslWarning();
        PageFactory.initElements(PageFactory.getWebDriver(), this);
        DriverExtension.waitUntilElementPresent(loginInput);
        loginInput.clear();
        loginInput.sendKeys(Props.get("sudir.login"));
        passwordInput.sendKeys(Props.get("sudir.password"));
        btnEntry.click();
        waitForPageToLoad(true);
        if(errorLoginTextExists(incorrectLoginSudir)){
            throw new Exception("Произошла ошибка авторизации СУДИР: Неверный логин или пароль");
        }
    }

    @ActionTitle("авторизуется с прямым логином и паролем")
    public void loginDirect() throws Exception{
        acceptSslWarning();
        PageFactory.initElements(PageFactory.getWebDriver(), this);
        DriverExtension.waitUntilElementPresent(loginInput);
        loginInput.clear();
        loginInput.sendKeys(Props.get("direct.login"));
        passwordInput.sendKeys(Props.get("direct.password"));
        btnEntry.click();
        waitForPageToLoad(true);
        if(errorLoginTextExists(incorrectLogin)){
            throw new Exception("Произошла ошибка авторизации: Неверный логин или пароль");
        }
    }

    private void acceptSslWarning() {
        List sslWarnings = getWebDriver().findElements(By.id("overridelink"));
        if (sslWarnings.size() != 0) {
            getWebDriver().navigate().to("javascript:document.getElementById(\'overridelink\').click();");
            waitForPageToLoad(true);
        }
    }
}
